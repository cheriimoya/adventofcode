# coding: utf-8
with open('input_unclean.txt') as file:
    lines = file.readlines()
lines.sort()

def getGuards(lines):
    guards = []
    for line in lines:
        lineWOtime = line[19:]
        if (lineWOtime[:5] == 'Guard'):
            guards.append(lineWOtime.split()[1])
    return guards

print(getGuards(lines))
