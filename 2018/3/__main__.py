# coding: utf-8
# %load 'code'
lines = open('input.txt').readlines()

arraySize = 1000

# initialize array
matrix = []
for i in range(arraySize):
    line = []
    for j in range(arraySize):
        line.append(0)
    matrix.append(line)

notoverlapping = 0

for line in lines:
    number = line.split()[0][1:]
    x, y = line.split()[2][:-1].split(',')
    x = int(x)
    y = int(y)
    w, h = line.split()[3].split('x')
    w = int(w)
    h = int(h)
    for i in range(w):
        for j in range(h):
            matrix[y+j][x+i] += 1
            

for line in lines:
    number = line.split()[0][1:]
    x, y = line.split()[2][:-1].split(',')
    x = int(x)
    y = int(y)
    w, h = line.split()[3].split('x')
    w = int(w)
    h = int(h)
    no = True
    for i in range(w):
        for j in range(h):
            if (matrix[y+j][x+i] != 1):
                no = False
    if (no):
        print(number)
        notoverlapping = number

overlap = 0
for y in range(arraySize):
    for x in range(arraySize):
        if (matrix[y][x] > 1):
            overlap += 1
            
print(overlap)
print(notoverlapping)
