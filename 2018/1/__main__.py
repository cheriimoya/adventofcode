# coding: utf-8
result = 0
lines = []
frequencies = [0]
twiceFrequency = None

with open('input.txt') as data:
    lines = data.readlines()

for line in lines:
    result += int(line)

print('part one: ',result)
result = 0

while (twiceFrequency == None):
    for line in lines:
        result += int(line)
        if (result in frequencies):
            twiceFrequency = result
            break
        else:
            frequencies.append(result)
        
print('part two: ',twiceFrequency)
