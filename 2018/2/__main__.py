lines = []
twoLetters = 0
threeLetters = 0

with open('input.txt') as data:
    lines = data.readlines()

for line in lines:
    checkedLetters = []
    gotTwo = False
    gotThree = False

    for letter in list(line):
        if (letter in checkedLetters):
            continue

        occurences = line.count(letter)
        
        if (occurences == 2 and not gotTwo):
            gotTwo = True
            twoLetters += 1
        elif (occurences == 3 and not gotThree):
            gotThree = True
            threeLetters += 1

        checkedLetters.append(letter)

print('part one:', twoLetters, '*', threeLetters, '=', twoLetters * threeLetters)

for line in lines:

